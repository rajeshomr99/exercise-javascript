var idno=0;
const schema = [
  {
    id: 1,
    name: "image",
    errmsg: "enter the proper image",
  },
  {
    id: 2,
    name: "organizationname",
    label: "Oranization Name",
    field: "select",
    type: "text",
    errmsg: "enter the organization name",
    Option: [
      { subname: "Vhitech" },
      { subname: "Google" },
      { subname: "Oracle" },
      { subname: "Facebook" },
    ],
  },
  {
    id: 3,
    name: "firstname",
    label: "First Name",
    type: "text",
    field: "input",
    max: 15,
    errmsg: "enter the valid name",
  },
  {
    id: 4,
    name: "lastname",
    label: "Last Name",
    type: "text",
    field: "input",
    max: 15,
    required: true,
    errmsg: "enter the valid name",
  },
  {
    id: 5,
    name: "dob",
    label: "Date Of Birth",
    type: "date",
    field: "input",
    max: "2015-09-13",
    min:"1915-09-13",
    required: true,
    errmsg: "enter the valid date of birth",
  },
  {
    id: 6,
    name: "gender",
    label: "Gender",
    type: "radio",
    field: "input",
    required: true,
    errmsg: "select any one in the field",
    Option: [
      {
        label: "Male",
      },
      {
        label: "Female",
      },  
    ],
  },
  {
    id: 7,
    name: "mobile_no",
    label: "Mobile Number",
    type: "text",
    max: 10,
    field: "input",
    required: true,
    errmsg: "enter the valid mobile number",
  },
  {
    id: 8,
    name: "email",
    label: "Email",
    type: "text",
    max: 50,
    field: "input",
    required: true,
    errmsg: "enter the valid email",
  },
  {
    id: 8,
    name: "country",
    label: "Country",
    type: "select",
    field: "select",
    required: true,
    Option: [
      {
        subname: "India",
        data: ["Tamil Nadu", "Goa", "Kerala"],
      },
      {
        subname: "USA",
        data: ["Texas", "Washington", "California"],
      },
      {
        subname: "United Kingdom",
        data: ["London", "Edinburgh", "Cardiff", "Belfast"],
      },
      {
        subname: "Germany",
        data: ["Berlin", "Bayern", "Sachsen", "Hessen"],
      },
    ],
    errmsg: "enter the valid name",
  },
  {
    id: 9,
    name: "state",
    label: "State",
    type: "text",
    field: "select",
    errmsg: "enter the valid state",
    Option: [],
  },
  {
    id: 9,
    name: "city",
    label: "City",
    type: "text",
    field: "input",
    max: 20,
    errmsg: "enter the valid state",
  },
];
const schema1 = [
  {
    id: 1,
    name: "address",
    label: "Address",
    Option: [
      {
        id: 1.1,
        name: "communicationaddress",
        label: "Communication Address",
        field: "textarea",
        errmsg: "enter the communication address",
      },
      {
        id: 1.2,
        name: "permanentaddress",
        label: "Permanent Address",
        field: "textarea",
        errmsg: "enter the permanent address",
      },
    ],
  },
  {
    id: 2,
    name: "checkbox",
  },
  {
    id: 3,
    name: "pincode",
    label: "Pincode",
    type: "text",
    max: 6,
    field: "input",
    errmsg: "enter the valid pincode",
  },
  {
    id: 4,
    name: "submitbutton",
  },
];
tableconten = [
  "S.No",
  "Name",
  "Gender",
  "DOB",
  "Mobile Number",
  "Email Id",
  "Edit/Delete",
];
function uploadimage(parent, item) {
  headName = document.createElement("div");
  fieldName = document.createElement("div");
  circle = document.createElement("div");
  image = document.createElement("img");
  upload = document.createElement("input");
  but = document.createElement("input");
  fieldName.id = item.name;
  headName.id=item.name+"main";
  circle.id = "circle";
  upload.id = "upload";
  image.id = "ima";
  upload.accept="image/png,image/jpeg";
  image.src = "./image/icon1.jfif";
  circle.appendChild(image);
  but.id = "upplod1";
  but.type = "button";
  but.value = "upload";
  upload.type = "file";
  upload.hidden = true;
  but.onclick = (e) => {
    headName.children[1].style.display="none";
    upload.click();
  };
  upload.addEventListener('change', function() {
   // value=upload.value.split('.')[0];
    var reader = new FileReader();
    reader.onload = function () {
      let output = document.getElementById("ima");
      //console.log(value)
      //console.log(reader.result.split('.'))
    //  let fileName= upload && upload.files[0] && upload.files[0].name;
      //let fileExtension = fileName.split('.').pop();
      //if(fileExtension==="jpg"){
        output.src = reader.result;
      //}
      
    };
    
    reader.readAsDataURL(upload.files[0]);
});
  fieldName.appendChild(circle);
  fieldName.appendChild(upload);
  fieldName.appendChild(but);
  headName.appendChild(fieldName)
  document.getElementById(parent).appendChild(headName);
}
//
function getitem(obj, err) {
  document.getElementById(err).children[2].style.display = "none";
  document.getElementById("citytextfield").value="";
  let date = document.getElementById("countrytextfield");
  if (date.selectedIndex !== 0) {
    document.getElementById("statetextfield").disabled = false;
    document.getElementById("citytextfield").disabled = false;
  } else {
    document.getElementById("statetextfield").disabled = true;
    document.getElementById("citytextfield").disabled = true;
    
  }
  let select1 = document.getElementById("statetextfield");
  i = 1;
  while (select1.length > 1) {
    select1.remove(select1.length - 1);
  }
  obj.map((item) => {
    if (item.subname === date.value) {
      addOption1(select1, item.data);
    }
  });
}
function addOption1(selectparent, arr) {
  arr.map((item) => {
    let option = document.createElement("option");
    option.innerHTML = item;
    selectparent.appendChild(option);
  });
}
//
function addbutton(parent, arr, err) {
  arr.map((item, i) => {
    label1 = document.createElement("label");
    radio = document.createElement("input");
    label.id = item.label + i;
    label.for = item.label + i;
    label1.innerHTML = item.label;
    radio.id = "radiobut"+i;
    radio.value = item.label;
    radio.name = "gender";
    radio.type = "radio";
    radio.onchange = (e) => {
      err.style.display = "none";
    };
    parent.appendChild(label1);
    parent.appendChild(radio);
  });
}

//create the option
function addOption(selectparent, arr) {
  arr.map((item) => {
    let option = document.createElement("option");
    option.innerHTML = item.subname;
    selectparent.appendChild(option);
  });
}
function displayprp(err) {
  document.getElementById(err).children[2].style.display = "none";
}
// create elment
function createElementTag(parent, item) {
  fieldName = document.createElement("div");  
  title = document.createElement("div");
  titlename = document.createElement("label");
  TextField = document.createElement(item.field);
  requridsym = document.createElement("span");
  errorMsg = document.createElement("div");
  fieldName.id = item.name;
  title.id = "label";
  titlename.id = "titlename";
  requridsym.id = "symbol";
  requridsym.innerHTML = "*";
  titlename.innerHTML = item.label;
  TextField.id = item.name + "textfield";
  TextField.type = item.type;
  errorMsg.id = "errormsg";
  errorMsg.innerHTML = item.errmsg;
  errorMsg.style.display = "none";
  TextField.max = item.max;
  if(item.name==="dob"){
    TextField.min = item.min;
  }
  TextField.oninput = (e) => {
    const element = document.getElementById(`${e.target.id}`);
    element.nextElementSibling.style.display = "none";
  };
  if (
    item.name === "lastname" ||
    item.name === "firstname" ||
    item.name === "city"
  ) {
    TextField.onkeypress = (event) => /[a-z]/i.test(event.key);
    TextField.maxLength = item.max;
  }
  if (item.name === "pincode" || item.name === "mobile_no") {
    TextField.onkeypress = (event) => /[0-9]/i.test(event.key);

    TextField.maxLength = item.max;
  }
  if (item.name === "state" || item.name === "city") {
    TextField.disabled = true;
  }
  if (item.name === "country") {
    TextField.onchange = function () {
      getitem(item.Option, item.name);
    };
  }
  if (item.Option) {
    let option = document.createElement("option");
    option.innerHTML = `select the ${item.label}`;
    TextField.appendChild(option);
    addOption(TextField, item.Option);
  }
  fieldName.appendChild(title);
  title.appendChild(titlename);
  title.appendChild(requridsym);
  if (item.type === "radio") {
    addbutton(fieldName, item.Option, errorMsg);
  } else {
    fieldName.appendChild(TextField);
  }

  fieldName.appendChild(errorMsg);
  document.getElementById(parent).appendChild(fieldName);
}
//address check


// create the tag for textarea
function createElementAddress(parent, item) {
  fieldName = document.createElement("div");
  title = document.createElement("div");
  titlename = document.createElement("label");
  requridsym = document.createElement("span");
  TextArea = document.createElement("textarea");
  errorMsg = document.createElement("div");
  fieldName.id = item.name;
  title.id = "label";
  titlename.id = "titlename";
  requridsym.id = "symbol";
  requridsym.innerHTML = "*";
  titlename.innerHTML = item.label;
  TextArea.id = item.name + "textarea";
  errorMsg.id = "errormsg";
  errorMsg.style.display = "none";
  TextArea.oninput = (e) => {
    const element = document.getElementById(`${e.target.id}`);
    element.nextElementSibling.style.display = "none";
    
  };

  errorMsg.innerHTML = item.errmsg;
  fieldName.appendChild(title);
  title.appendChild(titlename);
  title.appendChild(requridsym);
  fieldName.appendChild(TextArea);
  fieldName.appendChild(errorMsg);
  document.getElementById(parent).appendChild(fieldName);
}
function errorMesssage(parent){
  errorMsg = document.createElement("div");
  errorMsg.id="errormsg";
  errorMsg.style.display="none";
  errorMsg.innerHTML ="Upload the image";
  document.getElementById("imagemain").appendChild(errorMsg);
}
  

// create the tag for check box
function createCheckBox(parent, item) {
  fieldName = document.createElement("div");
  checkBox1 = document.createElement("input");
  spantag = document.createElement("span");
  fieldName.id = item.name;
  checkBox1.id = item.name + "tag";
  spantag.id = "content";
  checkBox1.type = "checkbox";
  checkBox1.onchange = function () {
    copyTextValue(this);
  };
  spantag.innerHTML = "Permanent Address same as Communication Address";
  fieldName.appendChild(checkBox1);
  fieldName.appendChild(spantag);
  document.getElementById(parent).appendChild(fieldName);
}
//check the book
function copyTextValue(bf) {
  let text1 = document.getElementById("communicationaddresstextarea").value;
  if (bf.checked) {
    document.getElementById("permanentaddresstextarea").disabled = true;
    document.getElementById("permanentaddresstextarea").value = text1;
  } else {
    document.getElementById("permanentaddresstextarea").disabled = false;
  }
}

// create the button
function createElementButton(parent, item) {
  fieldName = document.createElement("div");
  button1 = document.createElement("input");
  button2 = document.createElement("input");
  fieldName.id = item.name;
  button1.id = "submit";
  button2.id = "cancel";
  button1.type = "submit";
  button2.type = "reset";
  button1.value = "Register";
  button2.value = "Cancel";

  fieldName.appendChild(button2);
  fieldName.appendChild(button1);
  document.getElementById(parent).appendChild(fieldName);
}
let count = 0,
  i = 0;
schema.map((item) => {
  if (count % 3 === 0) {
    i++;
    userDetail1 = document.createElement("div");
    userDetail1.id = "userdetail" + i;
    document.getElementById("subcontainer1").appendChild(userDetail1);
  }
  temp = "userdetail" + i;
  if (item.name === "image") {
    uploadimage(temp, item);
    errorMesssage(temp);
  } else {
    createElementTag(temp, item);
  }

  let s = count++;
});

schema1.map((item) => {
  userDetail1 = document.createElement("div");
  userDetail1.id = item.name;
  document.getElementById("subcontainer2").appendChild(userDetail1);
  if (item.name === "address") {
    createElementAddress(item.name, item.Option[0]);
    createElementAddress(item.name, item.Option[1]);
  }
  if (item.name === "checkbox") {
    createCheckBox(item.name, item);
  }
  if (item.name === "pincode") {
    createElementTag(item.name, item);
  }
  if (item.name === "submitbutton") {
    createElementButton(item.name, item);
  }
});
//
inputBox = document.getElementById("communicationaddresstextarea");

inputBox.onkeyup = function () {
  if (document.getElementById("checkboxtag").checked == true)
    document.getElementById("permanentaddresstextarea").value = inputBox.value;
};
//date validation
function dob(date) {
  const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10);
  if (getAge(date) > 6 && getAge(date) < 80) {
    return false;
  }
  return true;
}
//mobile validation
function mob(date) {
  var mob = /^[1-9]{1}[0-9]{9}$/; 
  if (mob.test(date)) {
    return false;
  }   
  return true;
}
//email validation
function emailchecking(value) {
  let emailcheck = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
  if (emailcheck.test(value)) {
    const usersemail=JSON.parse(localStorage.getItem("userdetail"));
    if(usersemail!==null&& usersemail.length>0 ){
    if(document.getElementById("submit").value!=="Update"){
      let found=usersemail.some((user,i)=>{
      return user.email===value;
    });
    if(found){
      document.getElementById("email").children[2].innerHTML="email is already exist";
      return true;
    }
  }
  else{
    console.log(usersemail);
    let found = usersemail.some((user, i) => {
      console.log(idno + " " + i);
      if (idno !== i) {
        return user.email === value;
      }
    });
    if (found) {
      document.getElementById("email").children[2].innerHTML =
        "email is already exist";
      return true;
    }
  }
}
    return false;
  }
  
   
  return true;
}
//select tag validation organzition
function selectvalid(date) {
 
  if (date.selectedIndex !== 0) {
    return false;
  }
  return true;
}
//valid the pin
function pin(date) {
  var pincode1 = /^[1-9]{1}[0-9]{4}$/;
  if (pincode.test(date)) {
    return false;
  }
  return true;
}
function rad() {
  
  if (document.getElementById("radiobut0").checked == true||document.getElementById("radiobut1").checked == true) {
    document.getElementById("gender").children[5].style.display = "none";
  } else {
    document.getElementById("gender").children[5].style.display = "block";
    return false;
  }
}
function testaddress(arr) {
  let resultreturn;
  arr.map((item) => {
    let date = document.getElementById(item.name).children[1].value;
    if (!date.trim()) {
      document.getElementById(item.name).children[2].style.display = "block";
      resultreturn = false;
    } else {
      document.getElementById(item.name).children[2].style.display = "none";
    }
  });
  return resultreturn;
}

let check = true;
function register(event) {
  document.getElementById("email").children[2].innerHTML="Enter the valid email"
  check = true;
  event.preventDefault();
  schema.map((item) => {
    let tempvar = item.name;
      if(item.name==="image"){
        value=document.getElementById("ima").src;
        //console.log(value);
        if(document.getElementById("ima").src === "http://127.0.0.1:5500/CRUD/image/icon1.jfif"){
          document.getElementById("imagemain").children[1].style.display = "block";
          check = false;
        }
        else{
          document.getElementById("imagemain").children[1].style.display = "none";
        }
      }
    if (
      item.type !== "radio" &&
      item.field !== "select" &&
      item.name !== "image"
    ) {
      let date = document.getElementById(item.name).children[1].value;
      if (
        date.length === 0 || item.name === "mobile_no"? mob(date)
            : item.name === "email"
          ? emailchecking(date)
          : false
      ) {
        document.getElementById(item.name).children[2].style.display = "block";
        check = false;
      } else {
        document.getElementById(item.name).children[2].style.display = "none";
      }
    }
    if (item.type === "radio") {
      let temprad = rad();
      if (temprad === false) {
        check = false;
      }
    }
    if (item.field === "select") {
      let date = document.getElementById(item.name).children[1];
      if (selectvalid(date)) {
        document.getElementById(item.name).children[2].style.display = "block";
        check = false;
      } else {
        document.getElementById(item.name).children[2].style.display = "none";
      }
    }
  });

  schema1.map((item) => {
    if (item.name === "address") {
      
      let tempadd = testaddress(item.Option);
      if (tempadd === false) {
        check = false;
      }
    }
    if (item.name === "pincode") {
      let p1 = document.getElementById("pincode").children[0];
      let p2 = p1.children[2];
      text1 = document.getElementById("pincodetextfield").value;
      if (text1.length === 0 || text1.length !== 6) {
        p2.style.display = "block";
        check = false;
      } else {
        p2.style.display = "none";
      }
    }
  });
  addlocalstorage(check);
}
function addlocalstorage(item) {
  let userdetail =JSON.parse(localStorage.getItem("userdetail"))||[];
  
  let cart = {};
  if (item) {
    document.getElementById("subcontainer3").style.pointerEvents = "visible";
    schema.map((item) => {
      if(item.name==="image"){
        value=document.getElementById("ima").src;
        cart[item.name] = value;
      }

      if (item.name !== "image" && item.name !== "gender") {
        value = document.getElementById(item.name).children[1].value;
        cart[item.name] = value;
      }
      if (item.name === "gender") {
        if(document.getElementById("radiobut0").checked == true)
       {
         value = document.getElementById("radiobut0").value;
       }
       if(document.getElementById("radiobut1").checked == true)
       {
         value = document.getElementById("radiobut1").value;
       }
        cart[item.name] = value;
      }
    });

    schema1.map((item) => {
      if (item.name === "address") {
        value = document.getElementById("communicationaddresstextarea").value;
        cart.communicationaddress = value;
      }
      if (item.name === "address") {
        value = document.getElementById("permanentaddresstextarea").value;
        cart.permanentaddress = value;
      }
      if (item.name === "pincode") {
        value = document.getElementById("pincodetextfield").value;
        cart[item.name] = value;
      }
    });
    
    if( document.getElementById("submit").value=="Update"){
      userdetail[idno]={...cart}
      localStorage.setItem("userdetail", JSON.stringify(userdetail));

    }
    else{
      userdetail.push(cart);
      localStorage.setItem("userdetail", JSON.stringify(userdetail));
    }
    document.getElementById("ima").src="./image/icon1.jfif";  
      createcontent(JSON.parse(localStorage.getItem("userdetail")));
      document.getElementById("submit").value="Register";
      document.getElementById("statetextfield").disabled=true;
      document.getElementById("citytextfield").disabled=true;
    document.getElementById("fillform").reset();

  }
}
function createcontent( arr) {
  document.getElementById("tablebody").innerHTML="";
 if(arr.length>0){

 
  arr.map((item1,id)=>{

  
  let colname = document.createElement("tr");
  tableconten.map((item) => {
    let colname1 = document.createElement("td");
    if(item=== "S.No"){
      colname1.innerHTML =id+1;
    }
    if (item === "Name") {
      colname1.innerHTML = item1.firstname + " " + item1.lastname;
    }
    if (item === "Gender") {
      colname1.innerHTML = item1.gender;
    }
    if (item === "DOB") {
      colname1.innerHTML = item1.dob;
    }
    if (item === "Mobile Number") {
      colname1.innerHTML = item1.mobile_no;
    }
    if (item === "Email Id") {
      colname1.innerHTML = item1.email;
    }
    if (item === "Edit/Delete") {
      span1 = document.createElement("span");
      space = document.createElement("span");
      span2 = document.createElement("span");
      span1.id = "edit";
      space.innerHTML = "/";
      span1.innerHTML = "Edit";
      span2.id = "delete";
      span2.innerHTML = "Delete";
      colname1.style.color="blue";
      span1.onclick = (e) => {
        colname1.style.color="white";
        document.getElementById("subcontainer3").style.pointerEvents = "none";
        colname.style.color="white";
        colname.style.backgroundColor="firebrick";
        editdetails(id);

      };
      span2.onclick = (event) => {
        deleteElement(id);
      };
      colname1.appendChild(span1);
      colname1.appendChild(space);
      colname1.appendChild(span2);
    }
    colname.appendChild(colname1);
  });
  document.getElementById("tablebody").appendChild(colname);
});
 }
}
function createheader(parent) {
  row1 = document.createElement("tr");
  tableconten.map((item) => {
    col = document.createElement("th");
    col.innerHTML = item;
    row1.appendChild(col);
  });
  parent.appendChild(row1);
}
function createtable() {
  table = document.createElement("table");
  table.id = "maintable";
 // table.style.display="none";
  createheader(table);
  tablebody = document.createElement("tbody");
  tablebody.id="tablebody";
  table.appendChild(tablebody)
  document.getElementById("subcontainer3").appendChild(table);
}
createtable();



function editdetails(id) {
  idno=id;
  //document.getElementById("tablebody").children[id].children[7];
  document.getElementById("statetextfield").disabled=false;
    document.getElementById("citytextfield").disabled=false;
  let templocal = JSON.parse(localStorage.getItem("userdetail"));
    for (const [key, value] of Object.entries(templocal[id])) {
      if(key==="image"){
            document.getElementById('ima').src =value
      }
      else if(key==="pincode"){
        document.getElementById("pincodetextfield").value=value;
      }
      else if(key==="gender")
      {
        if(document.getElementById("radiobut0").value===value)
        {
          document.getElementById("radiobut0").checked = true
        }
        else{
          document.getElementById("radiobut1").checked = true
        }
      }
      else{
        document.getElementById(key).children[1].value=value;
      }
     
      
      
    }
    if(document.getElementById("communicationaddresstextarea").value === document.getElementById("permanentaddresstextarea").value){
      document.getElementById("checkboxtag").checked=true;
      document.getElementById("permanentaddresstextarea").disabled=true;  
    }
    document.getElementById("submit").value="Update";
  
}


function  deleteElement(selected_index){
  let  userdetail=JSON.parse(localStorage.getItem("userdetail"));
  userdetail.splice(selected_index, 1);
  localStorage.setItem("userdetail", JSON.stringify(userdetail));
  createcontent(JSON.parse(localStorage.getItem("userdetail")));
}

let usersdetail =JSON.parse(localStorage.getItem("userdetail"));
  if(usersdetail!==null){
    createcontent(JSON.parse(localStorage.getItem("userdetail")));
  }
document.getElementById("cancel").onclick=(e)=>{
  if(document.getElementById("submit").value==="Update"){
    document.getElementById("ima").src="./image/icon1.jfif";
    document.getElementById("subcontainer3").style.pointerEvents = "visible";
    document.getElementById("submit").value="Register";
    createcontent(JSON.parse(localStorage.getItem("userdetail")));
  }
}