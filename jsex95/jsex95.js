var data1 ='{"name": "saran", "age": 21}';
const data2 ={name: "saran", age: 21};  
function IsJsonString(str) {
    try {
      let json = JSON.parse(str);
      return (typeof json === 'object');
    } catch (e) {
      return false;
    }
  }
document.getElementById("result1").innerHTML = ` first object ${IsJsonString(data1)}`;
document.getElementById("result2").innerHTML = `second object ${IsJsonString(data2)}`;
    