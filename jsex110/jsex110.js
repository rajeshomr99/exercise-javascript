let data = [
    {
      id: 1,
      name: "rajesh",
      data: [
        {
          id: 1,
          name: "rajesh1",
        },
        {
          id: 2,
          name: "rajesh2",
        },
        {
          id: 3,
          name: "rajesh3",
        },
      ],
    },
    {
      id: 2,
      name: "saran",
      data: [
        {
          id: 1,
          name: "saran1",
        },
      ],
    },
    {
      id: 3,
      name: "rithi",
    },
    {
      id: 4,
      name: "pradeep",
      data: [
        {
          id: 1,
          name: "pradeep1",
          data: [
            {
              id: 1,
              name: "pradeep1.1",
            },
          ],
        },
        {
          id: 2,
          name: "pradeep1.2",
        },
        {
          id: 3,
          name: "pradeep1.3",
        },
      ],
    },
    {
      id: 5,
      name: "yogi",
    },
  ];
  
  function generateDropdown() {
    document.getElementById("dropdown").innerHTML="";
    generate(data, 1);
  }
  
  const elements = [];
  const visitedArray = [];
  
  // generated function
  
  function generate(data, idSet) {
    let i = 0;
    const array = [];
    let select = document.querySelector("#dropdown");
    console.log(select);
    let selectContainer;
    let selectList;
    let a = `selectContainer${idSet - 1}`;
    console.log(a);
    const selectContainerParent = document.getElementById(a);
    console.log(selectContainerParent);
    if (idSet == 1) {
      selectContainer = document.createElement("span");
      console.log(selectContainer);
      selectContainer.id = "selectContainer" + idSet;
      console.log(selectContainer.id);
      selectList = document.createElement("select");
      selectList.id = "mySelect" + idSet;
      selectContainer.appendChild(selectList);
      select.appendChild(selectContainer);
    } else {
      selectContainer = document.createElement("span");
      selectContainer.id = "selectContainer" + idSet;
      selectList = document.createElement("select");
      selectList.id = "mySelect" + idSet;
      selectContainer.appendChild(selectList);
      selectContainerParent.appendChild(selectContainer);
    }
    // Insert disabled select
  
    if (i === 0) {
      const option = document.createElement("option");
      option.value = "";
      option.text = "select";
      selectList.appendChild(option);
      option.setAttribute("disabled", "disabled");
      i++;
    }
  
    // insert other elements
  
    data.forEach((value) => {
      const option = document.createElement("option");
      option.value = value.id;
      // option.value = value.name;
      option.text = value.name;
      selectList.appendChild(option);
      array.push(value);
    });
  
    //   check data on change in the select
  
    selectList.onchange = function (e) {
      handleChange(e, array, idSet, selectContainer);
    };
  }
  
  // onchange funcion
  
  function handleChange(event, array, idSet, select) {
    let temp = parseInt(event.target.value);
    let obj = array.find((e) => e.id === temp);
  
    if (select.childNodes.length > 1) {
      select.removeChild(select.childNodes[1]);
    }
  
    let res = obj.hasOwnProperty("data");
    if (res) {
      idSet++;
      generate(obj.data, idSet);
    } else {
      console.log("retuning");
      return;
    }
  }
  
  