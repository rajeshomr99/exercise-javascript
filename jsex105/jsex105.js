    let graph = {
        start: { A: 5, B: 2 },
        A: { start: 1, C: 4, D: 2 },
        B: { A: 8, D: 7 },
        C: { D: 6, finish: 3 },
        D: { finish: 1 },
        finish: {},
    };
    let shortestDistanceNode = (distances, visited) => {
        let shortest = null;
            console.log(visited)
        for (let node in distances) {
            let currentIsShortest =
                shortest === null || distances[node] < distances[shortest];
                
                if (currentIsShortest && !visited.includes(node)) {
            shortest = node;
            }
        }
        return shortest;
    };
    let findShortestPath = (graph, startNode, endNode) => {
    
        let distances = {};
    distances[endNode] = "Infinity";
    distances = Object.assign(distances, graph[startNode]);
    console.log( distances)
    let parents = { endNode: null };
    for (let child in graph[startNode]) {
    
    console.log(parents[child] = startNode);
    console.log(parents[child] )
    }
    console.log(parents)
    let visited = [];
    let node = shortestDistanceNode(distances, visited);
        console.log(node)
    while (node) {
    let distance = distances[node];
    let children = graph[node]; 
    console.log(distance)
    console.log(children)
        
        for (let child in children) {
    
            if (String(child) === String(startNode)) {
            continue;
        } else {
            let newdistance = distance + children[child];
            if (!distances[child] || distances[child] > newdistance) {

        distances[child] = newdistance;

        parents[child] = node;
        console.log(distances)
        console.log(parents)
        } 
            }
        }  
        
        visited.push(node);

        console.log(node = shortestDistanceNode(distances, visited));
        }
    
    
    let shortestPath = [endNode];
    console.log(shortestPath)
    let parent = parents[endNode];
    console.log(parent)
    while (parent) {
    console.log(shortestPath.push(parent));
    parent = parents[parent];
    }
    shortestPath.reverse();
    
    let results = {
    distance: distances[endNode],
    path: shortestPath,
    };
    
    return results;
    };
    console.log(findShortestPath(graph, "start", "finish"));