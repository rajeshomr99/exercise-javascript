function check() {
    let input1 =Number(document.getElementById("inputString1").value);
    let input2 =Number(document.getElementById("inputString2").value);
    if(!input1 ||!input2 ){
        alert("enter the number");
        return;
    }
    let gcd=1;

    for (let i = 1; i <= input1 && i <= input2; i++) {
        if( input1 % i == 0 && input2 % i == 0) {
            gcd = i;
        }
    }
    document.getElementById("result1").innerHTML=gcd;
}