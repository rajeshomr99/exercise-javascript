var maze = [
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [1, 1, 1, 0, 1, 0, 0, 1, 1, 1],
    [0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    [0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 2]
];

function Maze(maze) {
let ans=false;
    function walk(column, row) {
      
        if(maze[column][row] == 2) {
       		ans= true ;
        } else if(maze[column][row] == 0) {
          
            maze[column][row] = 9;
            if(column < maze.length - 1) {
                walk(column + 1, row);
            }
            if(row < maze[column].length - 1) {
                walk(column, row + 1);
            }
            if(column > 0) {
                walk(column - 1, row);
            }
            if(row > 0) {
                walk(column, row - 1);
            }
        }
    };
	 walk(0,0);
     if(ans){
         console.log("valid ");
     }
     else{
         console.log("no valid");
     }

};

Maze(maze);