const array=[];
function add() {
    let input1 =Number(document.getElementById("inputString1").value);
    if(!input1 ){
        alert("enter the number");
        return;
    }
    array.push(input1);
    document.getElementById("result1").innerHTML=JSON.stringify(array);
}
function check() {
    let input2 =Number(document.getElementById("inputString2").value);
    if(!input2 ){
        alert("enter the number");
        return;
    }
    function listToMatrix(list, count) {
        var matrix = [], i, k;
    
        for (i = 0, k = -1; i < list.length; i++) {
            if (i % count === 0) {
                k++;
                matrix[k] = [];
            }
    
            matrix[k].push(list[i]);
        }
    
        return matrix;
    }

    document.getElementById("result2").innerHTML=JSON.stringify(listToMatrix(array,input2));

}