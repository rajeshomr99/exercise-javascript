function check() {
    let radius =Number(document.getElementById("inputString1").value);
    if (!radius) {
      alert("enter the the number");
      return;
    }
    document.getElementById("result").innerHTML=`Area of circle ${(Math.PI * radius * radius).toFixed(2)} <br> perimeter of circle ${(2 * Math.PI * radius).toFixed(2)}`
}
