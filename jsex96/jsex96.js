function check() {
    let string =document.getElementById("inputString1").value;
    if(!string){
        alert("enter the string");
        return;
    }
    function isRegExp(regExp){
        try {
              new RegExp(regExp);
            } catch(e) {
              return false;
            }
       return true;
    }

    document.getElementById("result").innerHTML=isRegExp(string);
 }
 