const wildcard = `<div class="cardSpecialInnerWild">
  <div class="cardSpecialTopWild">
    <div class="cardSpecialColors1Wild"></div>
  </div>
  <div class="cardSpecialNumberWild">
    <div class="cardSpecialColors2Wild"></div>
  </div>
  <div class="cardSpecialBottomWild">
    <div class="cardSpecialColors1Wild"></div>
  </div>
</div>`;

const draw4card = `<div class="cardInnerSpecialFourWild">
  <div class="cardTop">+4</div>
  <div class="cardNumberSpecialFourWild">
    <div class="fourWildBox1"></div>
    <div class="fourWildBox2"></div>
    <div class="fourWildBox3"></div>
    <div class="fourWildBox4"></div>
  </div>
  <div class="cardBottom">+4</div>
</div>`;
let color = ["red", "yellow", "blue", "green"];
let number = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  "skip",
  "reverse",
  "+2",
  "draw4",
  "wildCard",
];
let deckCard = new Array();
let current,
  buttunon = true,
  keepplaycheck = false;
let resultparent = document.getElementById("result");
// create array if object
function getDeck() {
  color.map((item1) => {
    number.map((item2) => {
      var card = { colour: item1, number: item2 };
      if (item2 !== 0 && item2 !== "draw4" && item2 !== "wildCard") {
        deckCard.push(card);
      }
      deckCard.push(card);
    });
  });

  return deckCard;
}
deckCard = getDeck();

//start function()
let last;
let i;
let t = 0;
let cpuMachine = [],
  pp = [];
function start() {
  document.getElementById("container").style.pointerEvents = "visible";
  document.getElementById("cpu").innerHTML = "";
  document.getElementById("deck").innerHTML = "";
  document.getElementById("player").innerHTML = "";

  //shuffle

  function shuffle() {
    for (var i = 0; i < 1000; i++) {
      var location1 = Math.floor(Math.random() * deckCard.length);
      var location2 = Math.floor(Math.random() * deckCard.length);
      var tmp = deckCard[location1];

      deckCard[location1] = deckCard[location2];
      deckCard[location2] = tmp;
    }
  }
  shuffle();

  // start the assign card to user  and cpu
  cpuMachine = [];
  pp = [];
  for (i = 0; i < 14; i++) {
    if (i % 2 == 0) {
      pp.push(deckCard[i]);
    } else {
      cpuMachine.push(deckCard[i]);
    }
    if (i === 13) {
      last = deckCard[i + 1];
    }
  }
  if (
    last.number === "wildCard" ||
    last.number === "draw4" ||
    last.number === "+2" ||
    last.number === "skip" ||
    last.number === "reverse"
  ) {
    start();
  }
}
start();
for (let j = 0; j < 7; j++) {
  frontSideCard("player", pp[j]);
  backSideCard("cpu");
}

current = last;
console.log(current);
// create card samefunction
function createCard(cardTop, cardNumber, cardBottom, item) {
  cardTop.innerHTML = item;
  cardBottom.innerHTML = item;
  cardNumber.innerHTML = item;
}
//create function for frontened of the card
function frontSideCard(s, last) {
  let card = document.createElement("div");
  let cardsub = document.createElement("div");
  let cardTop = document.createElement("div");
  let cardBottom = document.createElement("div");
  let cardNumber = document.createElement("div");
  card.className = "card";
  if (last.number === "wildCard") {
    card.innerHTML = wildcard;
    card.style.backgroundColor = "black";
  } else if (last.number === "draw4") {
    card.innerHTML = draw4card;
    card.style.backgroundColor = "black";
  } else {
    cardsub.className = "cardsub";
    cardTop.className = "cardTop";
    cardBottom.className = "cardBottom";
    cardNumber.className = "cardNumber";
    card.style.backgroundColor = last.colour;
    if (last.number === "skip") {
      createCard(cardTop, cardNumber, cardBottom, `<i class="fas fa-ban"></i>`);
    } else if (last.number === "reverse") {
      createCard(
        cardTop,
        cardNumber,
        cardBottom,
        `<i class="fas fa-exchange-alt"></i>`
      );
    } else {
      createCard(cardTop, cardNumber, cardBottom, last.number);
    }
    cardsub.appendChild(cardTop);
    cardsub.appendChild(cardNumber);
    cardsub.appendChild(cardBottom);
    card.appendChild(cardsub);
  }
  card.onclick = function () {
    const index = [...card.parentElement.children].indexOf(card);
    uno(index, last);
  };
  document.getElementById(s).appendChild(card);
}
// create back side of the card
function backSideCard(s) {
  var card = document.createElement("div");
  var subcard = document.createElement("div");
  var content = document.createElement("div");
  card.className = "cardBack";
  subcard.className = "cardBackInner";
  content.className = "cardBackNumber";
  content.innerHTML = "UNO";
  subcard.appendChild(content);
  card.appendChild(subcard);
  document.getElementById(s).appendChild(card);
}
// create click button
function clickButton(s) {
  let card = document.createElement("button");
  card.id = "but";
  // card.disabled=true;
  card.innerHTML = "uno";
  card.onclick = (e) => {
    if (pp.length === 2) {
      buttunon = false;
    }
  };
  document.getElementById(s).appendChild(card);
}
// create in middle of the screen
function renderDeck2() {
  backSideCard("deck");
  frontSideCard("deck", last);
  clickButton("deck");
}
renderDeck2();
//document.getElementById("myBtn")
document.getElementById("deck").children[1].style.pointerEvents = "none";
document.getElementById("but").disabled = true;
// delete element in in the array
function deleteElement(last, s) {
  let temp = s.findIndex((item) => item === last);
  s.splice(temp, 1);
}

// add two card cpu and player
function addtwocard(n, checkno) {
  for (let k = 0; k < checkno; k++) {
    i++;
    if (n == 0) {
      cpuMachine.push(deckCard[i]);
      backSideCard("cpu");
    } else {
      pp.push(deckCard[i]);
      frontSideCard("player", deckCard[i]);
    }
  }
}
// calculate score
function total1(s) {
  let total = 0;
  if (s.length === 0) {
    return total;
  }
  s.map((item) => {
      total += (item.number === "wildCard")?50:(item.number === "draw4")?50:(item.number === "+2")?20:(item.number === "skip")?20:(item.number === "reverse")?20:item.number;
  });
  return total;
}
// create result for the card
function checkFinalResult() {
  if (pp.length === 0 || cpuMachine.length === 0) {
    resultparent.style.display = "block";
    resultparent.children[0].innerHTML =
      cpuMachine.length === 0 ? "cpu is win" : "cpu is lose";
    resultparent.children[1].innerHTML = "score is " + " " + total1(cpuMachine);
    resultparent.children[2].innerHTML =
      pp.length === 0 ? "player is win" : "player is lose";
    resultparent.children[3].innerHTML = "score is " + total1(pp);
    if (pp.length === 0) {
      document.getElementById("cpu").innerHTML = "";
      for (let p = 0; p < cpuMachine.length; p++) {
        frontSideCard("cpu", cpuMachine[p]);
      }
    }
    document.getElementById("container").style.pointerEvents = "none";
  }
}

//delete card
function removeElement(s, index) {
  let list = document.getElementById(s);
  list.removeChild(list.childNodes[index]);
}
// change the colour the card
function cardChange(last) {
  let parent = document.getElementById("deck");
  let child = parent.children[1];
  let subparent = child.children[0];
  child.style.backgroundColor = last.colour;
  itemnumber = last.number;
  if (last.number === "skip") {
    itemnumber = `<i class="fas fa-ban"></i>`;
  }
  if (last.number === "reverse") {
    itemnumber = `<i class="fas fa-exchange-alt"></i>`;
  }
  if (last.number === "wildCard"||last.number === "draw4") {
    itemnumber = "";
  }
  subparent.children[0].innerHTML = itemnumber;
  subparent.children[1].innerHTML = itemnumber;
  subparent.children[2].innerHTML = itemnumber;
}
// create same function option of co
function samefuctioncolor(last, check2) {
  cardChange(last);
  current = last;
  check2.style.display = "none";
  
  document.getElementById("deck").style.pointerEvents = "visible"
  if(last.number==="wildCard" && pp.length!==0)
  {
    setTimeout(checlCpumachine, 2000);
  }
  else{
//    document.getElementById("player").style.pointerEvents = "visible";

    document.getElementById("player").style.pointerEvents = "visible";
  }
}
//create option of colour
function checkOption(last) {
  document.getElementById("player").style.pointerEvents = "none"
  document.getElementById("deck").style.pointerEvents = "none"

  let check2 = document.getElementById("main");
  check2.style.display = "flex";
  check2.children[0].onclick = (e) => {
    last.colour = "green";
    samefuctioncolor(last, check2);
  };
  check2.children[1].onclick = (e) => {
    last.colour = "red";
    samefuctioncolor(last, check2);
  };
  check2.children[2].onclick = (e) => {
    last.colour = "blue";
    samefuctioncolor(last, check2);
  };
  check2.children[3].onclick = (e) => {
    last.colour = "yellow";
    samefuctioncolor(last, check2);
  };
}
//create same function player
function samefunctionplayer(last, index) {
  if (pp.length === 2) {
    addtwocard2();
  }
  cardtext("player1", last, 1);
  cardChange(last);
  deleteElement(last, pp);
  removeElement("player", index);
  current = last;
  checkFinalResult();
}
function cardtext(plaplay, last, s) {
  if (s == 1) {
    document.getElementById("detail").innerHTML =`${plaplay} is  down the ${(last.number==="wildCard")?" ":(last.number==="draw4")?" ":last.colour} ${(last.number==="wildCard")?"wild":(last.number==="draw4")?"draw4":last.number} card"`;
  } else {
    document.getElementById("detail").innerHTML =
      plaplay + " is  get the card from deck";
  }
}

// create function for player
function checkplayer(index, last) {
  let check = false;
  document.getElementById("deck").style.pointerEvents = "visible";
  if (pp.length === 2) {
    document.getElementById("but").disabled = false;
  }
  if (last.number === "wildCard" || last.number === "draw4") {
    if (last.number === "draw4") {
      addtwocard(0, 4);
    }
    if (pp.length === 2) {
      addtwocard2();
    }
    cardtext("player1", last, 1);
    deleteElement(last, pp);
    removeElement("player", index);
    if (pp.length === 2) {
        document.getElementById("but").disabled = false;
      }
    checkOption(last);
  } else if (
    (current.colour === last.colour && last.number === "+2") ||
    (current.number === "+2" && last.number === "+2")
  ) {
    addtwocard(0, 2);
    samefunctionplayer(last, index);
    if (pp.length === 2) {
        document.getElementById("but").disabled = false;
      }
    console.log(current, "+2");
  } else if (
    (current.colour === last.colour && last.number === "skip") ||
    (current.number === "skip" && last.number === "skip") ||
    (current.colour === last.colour && last.number === "reverse") ||
    (current.number === "reverse" && last.number === "reverse")
  ) {
    samefunctionplayer(last, index);
    if (pp.length === 2) {
        document.getElementById("but").disabled = false;
      }
    console.log(current, "sr");
  } else if (current.colour === last.colour) {
    samefunctionplayer(last, index);
    check = true;
    console.log(current, "c");
  } else if (current.number === last.number) {
    samefunctionplayer(last, index);
    check = true;
    console.log(current, "num");
  }
  //card from the deck
  else {
    check = false;
  }
  return check;
}
// function keep play
function keepplaycard() {
  document.getElementById("keepplay").style.display = "block";
  document.getElementById("player").style.pointerEvents = "none";
  keepplaycheck = true;
  if (pp.length === 2) {
    document.getElementById("but").disabled = false;
  }
}
document.getElementById("keepplay").children[0].onclick = (e) => {
  if (keepplaycheck) {
    document.getElementById("keepplay").style.display = "none";
    setTimeout(checlCpumachine, 2000);
  }
};
document.getElementById("keepplay").children[1].onclick = (e) => {
  if (keepplaycheck) {

    uno(pp.length-1, pp[pp.length-1]) ;
    document.getElementById("keepplay").style.display = "none";
    document.getElementById("player").style.pointerEvents = "visible";
  }
};

// onclick card from for player
let parent = document.getElementById("deck");
let child1 = parent.children[0];
child1.onclick = (e) => {
  document.getElementById("deck").style.pointerEvents = "none";
  let cheaKpp = pp.filter((item) => {
    return (
      item.colour === current.colour ||
      item.number === current.number ||
      item.number === "wildCard" ||
      item.number === "draw4"
    );
  });
  if (cheaKpp.length === 0 && pp.length !== 0) {
    cardtext("player1", last.number, 0);
    addtwocard(1, 1);
    let cheaKpp13 = pp.filter((item) => {
      return (
        item.colour === current.colour ||
        item.number === current.number ||
        item.number === "wildCard" ||
        item.number === "draw4"
      );
    });
    if (cheaKpp13.length === 0) {
      setTimeout(checlCpumachine, 2000);
    } else {
        keepplaycard();
    }
  }
};
// btton clic the action
function addtwocard2() {
  if (buttunon) {
    addtwocard(1, 2);
  }
  buttunon = true;
}
// create  the same function in cpu
function samefuctioncpu(value1) {
  cardtext("player2", value1, 1);
  deleteElement(value1, cpuMachine);
  removeElement("cpu", 0);
  if (value1.number === "draw4" || value1.number === "wildCard") {
    value1.number = "";
    let temp11 = Math.floor(Math.random() * color.length);
    value1.colour = color[temp11];
  }
  current = value1;
  cardChange(value1);
  document.getElementById("player").style.pointerEvents = "visible";
}

function checlCpumachine() {
    document.getElementById("but").disabled = true;
  let cpucheack = cpuMachine.filter((item) => {
    return (
      item.colour === current.colour ||
      item.number === current.number ||
      item.number === "wildCard" ||
      item.number === "draw4"
    );
  });
  console.log(cpucheack);
  if (cpucheack.length > 0) {
    let tempp = Math.floor(Math.random() * cpucheack.length);
    console.log(cpucheack[tempp]);
    let s=cpucheack[tempp].number;
    if (
      cpucheack[tempp].number === "wildCard" ||
      cpucheack[tempp].number === "draw4"
    ) {
      if (cpucheack[tempp].number === "draw4") {
        addtwocard(1, 4);
      }
      samefuctioncpu(cpucheack[tempp]);
      if (cpuMachine.length !== 0 && s!=="wildCard" ) {
        setTimeout(checlCpumachine, 2000);
      }
      if(s ==="wildCard"){
        document.getElementById("deck").style.pointerEvents = "visible";
      }
      checkFinalResult();
    } else if (cpucheack[tempp].number === "+2") {
      cardtext("player2", cpucheack[tempp], 1);
      addtwocard(1, 2);
      samefuctioncpu(cpucheack[tempp]);
      if (cpuMachine.length !== 0) {
        setTimeout(checlCpumachine, 2000);
      }
      checkFinalResult();
    } else if (
      cpucheack[tempp].number === "skip" ||
      cpucheack[tempp].number === "reverse"
    ) {
      samefuctioncpu(cpucheack[tempp]);
      if (cpuMachine.length !== 0) {
        setTimeout(checlCpumachine, 2000);
      }
      checkFinalResult();
    } else {
      samefuctioncpu(cpucheack[tempp]);
      checkFinalResult();
      document.getElementById("deck").style.pointerEvents = "visible";
    }
  } else {
    cardtext("player2", "0", 0);
    addtwocard(0, 1);
    let cpucheack1 = cpuMachine.filter((item) => {
      return (
        item.colour === current.colour ||
        item.number === current.number ||
        item.number === "wildCard" ||
        item.number === "draw4"
      );
    });
    if (cpucheack1.length > 0) {
      setTimeout(checlCpumachine, 2000);
    } else {
      document.getElementById("deck").style.pointerEvents = "visible";
      document.getElementById("player").style.pointerEvents = "visible";

    }
  }
  if (pp.length === 2) {
    document.getElementById("but").disabled = false;
  }
}

function uno(index, last) {
    document.getElementById("but").disabled = true;
    let temp = checkplayer(index, last);


  if (temp) {
    document.getElementById("player").style.pointerEvents = "none";
    document.getElementById("deck").style.pointerEvents = "none";
    if (pp.length !== 0) {
      setTimeout(checlCpumachine, 2000);
    }
  }
}
