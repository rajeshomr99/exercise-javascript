array=[];
function add() {
    let input1 =Number(document.getElementById("inputString1").value);
    if(!input1 ){
        alert("enter the number");
        return;
    }
    array.push(input1);
    array.sort(function(a, b){return a-b});
    document.getElementById("result1").innerHTML=array;
}
function check() {
    let input2 =Number(document.getElementById("inputString2").value);
    if(!input2 || array.length < 0){
        alert("enter the number");
        return;
    }
    let recursiveFunction = function (arr, x, start, end) {
        if (start > end) return false;
        let mid=Math.floor((start + end)/2);
        console.log(mid)
       
        if (arr[mid]===x) return true;
              
        if(arr[mid] > x) 
            return recursiveFunction(arr, x, start, mid-1);
        else
      
            return recursiveFunction(arr, x, mid+1, end);
    }
    document.getElementById("result2").innerHTML=recursiveFunction(array,input2,0,array.length-1);   
    
}