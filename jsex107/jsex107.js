function check() {
    let pass =document.getElementById("inputString1").value;
    let passcheck=/^[a-zA-Z0-9*]{6,20}$/;    
    function allCharactersSame(s)
    {
        let n = s.length;
        for (let i = 0; i < n; i++)
       {
        let count=0;
        for(let j=i+1;j<n;j++)
        {
            if (s[i] === s[j])
                return count++;
        }
        if(count >= 3){
            return false;
        }
    }
    return true;
    }
    if (!pass) {
      alert("enter the the string");
      return;
    }
    document.getElementById("result1").innerHTML= pass.match(passcheck)&&allCharactersSame(pass)?"valid":"not valid";
    }