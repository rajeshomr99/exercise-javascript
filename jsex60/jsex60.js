function check() {
  let number = Number(document.getElementById("inputString1").value);
  if (!number) {
    alert("enter the the value");
    return;
  }
  let output = "";
  for (let i = 2; i <= number; i++) {
    let isPrime = (num) => {
      for (let i = 2; i < num; i++) if (num % i === 0) return false;
      return num > 1;
    }
    if (isPrime(i)) {
      output += i + "<br>";
    }
  }
  document.getElementById("result1").innerHTML = output;
}
